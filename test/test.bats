#!/usr/bin/env bats

setup() {
    DOCKER_IMAGE=${DOCKER_IMAGE:="test/azure-arm-deploy"}

    # generated
    RANDOM_NUMBER=$RANDOM

    # required globals - stored in Pipelines repository variables
    AZURE_APP_ID="${AZURE_APP_ID}"
    AZURE_PASSWORD="${AZURE_PASSWORD}"
    AZURE_TENANT_ID="${AZURE_TENANT_ID}"

    # required globals - generated
    AZURE_RESOURCE_GROUP="test${RANDOM_NUMBER}"

    AZURE_LOCATION="westus2"

    # optional globals - fixed
    AZURE_DEPLOYMENT_TEMPLATE_FILE="test/arm-templates/deployment.json"

    # optional globals - generated    
    AZURE_APP_NAME="website${RANDOM_NUMBER}"
    HOSTING_PLAN_NAME="${AZURE_APP_NAME}plan"
    AZURE_DEPLOYMENT_PARAMETERS="test/arm-templates/deployment.parameters.json --parameters {\"webSiteName\":{\"value\":\"${AZURE_APP_NAME}\"},\"hostingPlanName\":{\"value\":\"${HOSTING_PLAN_NAME}\"}}"

    echo "Building image..."
    docker build -t ${DOCKER_IMAGE}:0.1.0 .
}

teardown() {
    echo "Clean up Resource Group"
    az login --service-principal --username ${AZURE_APP_ID} --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}
    az group delete -n ${AZURE_RESOURCE_GROUP} --yes --no-wait
}

@test "ARM template can be deployed to Azure" {
    run docker run \
        -e AZURE_APP_ID="${AZURE_APP_ID}" \
        -e AZURE_PASSWORD="${AZURE_PASSWORD}" \
        -e AZURE_TENANT_ID="${AZURE_TENANT_ID}" \
        -e AZURE_LOCATION="${AZURE_LOCATION}" \
        -e AZURE_RESOURCE_GROUP="${AZURE_RESOURCE_GROUP}" \
        -e AZURE_DEPLOYMENT_TEMPLATE_FILE="${AZURE_DEPLOYMENT_TEMPLATE_FILE}" \
        -e AZURE_DEPLOYMENT_PARAMETERS="${AZURE_DEPLOYMENT_PARAMETERS}" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:0.1.0

    echo ${output}
    [[ "$status" -eq 0 ]]
    [[ "${output}" == *"Deployment successful."* ]]

    run curl --silent "https://${AZURE_APP_NAME}.azurewebsites.net"

    echo ${output}
    [[ "${status}" -eq 0 ]]
    [[ "${output}" == *"Your App Service app is up and running"* ]]
}

