# Bitbucket Pipelines Pipe: Azure ARM Deploy

Deploy resources to Azure using [Azure Resource Manager](https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-overview) templates. Azure Resource Manager is the deployment and management service for Azure. [Resource Manager templates](https://docs.microsoft.com/en-us/azure/templates/) are JSON files that define the resources you need to deploy for your solution.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: microsoft/azure-arm-deploy:1.0.2
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      AZURE_RESOURCE_GROUP: '<string>'
      # AZURE_LOCATION: '<string>' # Optional
      # AZURE_DEPLOYMENT_NAME: '<string>' # Optional
      # AZURE_DEPLOYMENT_MODE: '<string>' # Optional
      # AZURE_DEPLOYMENT_TEMPLATE_FILE: '<string>' # Optional
      # AZURE_DEPLOYMENT_TEMPLATE_URI: '<string>' # Optional
      # AZURE_DEPLOYMENT_PARAMETERS: '<string>' # Optional
      # AZURE_DEPLOYMENT_NO_WAIT: '<boolean>' # Optional
      # AZURE_DEPLOYMENT_ROLLBACK_ON_ERROR: '<boolean>' # Optional
      # DEBUG: '<boolean>' # Optional
```

## Variables

| Variable              | Usage                                                       |
| ------------------------------ | ----------------------------------------------------------- |
| AZURE_APP_ID (*)               | The app ID, URL or name associated with the service principal required for login. |
| AZURE_PASSWORD (*)             | Credentials like the service principal password, or path to certificate required for login. |
| AZURE_TENANT_ID  (*)           | The AAD tenant required for login with the service principal. |
| AZURE_RESOURCE_GROUP (*)       | Name of the resource group. |
| AZURE_LOCATION                 | Azure location. Required if AZURE_RESOURCE_GROUP doesn't exist already. |
| AZURE_DEPLOYMENT_NAME          | The deployment name. Default to template file base name. |
| AZURE_DEPLOYMENT_MODE          | The deployment mode. Accepted values: Incremental (only add resources to resource group) or Complete (remove extra resources from resource group). Default: Incremental. |
| AZURE_DEPLOYMENT_TEMPLATE_FILE | A template file path in the file system. |
| AZURE_DEPLOYMENT_TEMPLATE_URI  | A uri to a remote template file. |
| AZURE_DEPLOYMENT_PARAMETERS    | Deployment parameter values from a local JSON file, a remote JSON file, a JSON string and/or key/value pairs. |
| AZURE_DEPLOYMENT_ROLLBACK_ON_ERROR | Roll back to the last successful deployment on error. Default: `false` |
| AZURE_DEPLOYMENT_NO_WAIT       | Do not wait for the long-running operation to finish. Default: `false`. |
| DEBUG                          | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Prerequisites

You will need to configure a service principal with access to your resources. The easiest way to do it is by using the Azure cli. You can either [install the Azure cli](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest) on your local machine, or you can use the [Azure Cloud Shell](https://docs.microsoft.com/en-us/azure/cloud-shell/overview) provided by the Azure Portal in a browser.

### Instructions

Create a service principal in your Azure subscription:

```sh
az ad sp create-for-rbac --name MyServicePrincipal
```

### Documentation

* [Create an Azure service principal with Azure CLI](https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli)

## Examples

### Basic example

```yaml
script:
  - pipe: microsoft/azure-arm-deploy:1.0.2
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      AZURE_RESOURCE_GROUP: 'my-resource-group'
      AZURE_DEPLOYMENT_TEMPLATE_FILE: 'deployment.json'
      AZURE_DEPLOYMENT_PARAMETERS: 'deployment.parameters.json'
```

### Advanced example:

```yaml
script:
  - pipe: microsoft/azure-arm-deploy:1.0.2
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      AZURE_RESOURCE_GROUP: 'my-resource-group'
      AZURE_LOCATION: 'CentralUS'
      AZURE_DEPLOYMENT_NAME: 'my-deployment'
      AZURE_DEPLOYMENT_MODE: 'Complete'
      AZURE_DEPLOYMENT_TEMPLATE_URI: 'https://myresource/azuredeploy.json'
      AZURE_DEPLOYMENT_PARAMETERS: 'https://mysite/params.json --parameters SomeValue=This SomeOtherValue=That'
      AZURE_DEPLOYMENT_ROLLBACK_ON_ERROR: 'true'
      AZURE_DEPLOYMENT_NO_WAIT: 'true'
      DEBUG: 'true'
```

## Support

This sample is provided "as is" and is not supported. Likewise, no commitments are made as to its longevity or maintenance. To discuss this sample with other users, please visit the Azure DevOps Services section of the Microsoft Developer Community: https://developercommunity.visualstudio.com/spaces/21/index.html.
